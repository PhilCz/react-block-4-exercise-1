import logo from "./logo.svg";
import "./App.css";
import React from 'react';

class Counter extends React.Component {

  state = {
    clicks: 1, 
    counter: 0,
  }

handleClick = () => {
    let { counter, clicks } = this.state;
    this.setState({ clicks: clicks + 1 });
    if (this.state.clicks % 2 !== 0) {
      this.setState({ counter: counter + 1 });
      console.log (this.state);
    }
  };


  render() {
    return (
      <div className = 'view'>
        <div className='counter'>{this.state.counter}</div>
          <button onClick={this.handleClick}>+</button>
      </div>
    );
  }
}


export default Counter;
